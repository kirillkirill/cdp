Lambda simple task java8

    Create class Point, that contains coordinates fields x and y.
    
    Create list of 10 instans of class Point, those set by random coordinates.
    
    Use lampdas to iterate list and print to stdout each point from list.
    
    Map elements to class NewPoint that consist fields a and b.
    
    Filter elements with coordinates values less than 3.
    
    Remove duplicates.
    
    Calculate sum all elements coordinates.
    
    Calculate multplication all elements coordinates.


to run:

    git clone https://bitbucket.org/kirillkirill/cdp.git
    
    cd cdp/cdp-streams/
    
    mvn test