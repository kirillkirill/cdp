import by.br.kir.streams.NewPoint;
import by.br.kir.streams.Point;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kiryl_c on 5/30/17.
 */
public class StreamTest {

    @Test
    public void test() {
        final int count = 10;
        final int filterLimit = 3;
        final Random random = new Random();
        List<Point> pointList = IntStream
            .range(0, count)
            .mapToObj(value -> new Point(random.nextInt(), random.nextInt()))
            .collect(Collectors.toList());

        Assert.assertEquals(count, pointList.size());

        pointList
            .stream()
            .forEach(point -> System.out.println("point = " + point));

        List<NewPoint> newPointList = pointList
            .stream()
            .map(point -> new NewPoint(point.getX(), point.getY()))
            .filter(newPoint -> newPoint.getA() < filterLimit && newPoint.getB() < filterLimit)
            .distinct()
            .collect(Collectors.toList());

        newPointList
            .stream()
            .mapToInt(value -> value.getA() + value.getB())
            .reduce((i1, i2) -> i1 + i2)
            .ifPresent(sum -> System.out.println("sum = " + sum));

        newPointList
            .stream()
            .mapToInt(value -> value.getA() * value.getB())
            .reduce((i1, i2) -> i1 * i2)
            .ifPresent(multiplication -> System.out.println("multiplication = " + multiplication));
    }
}
