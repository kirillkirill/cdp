package by.br.kir.streams;

/**
 * Created by kiryl_c on 5/30/17.
 */
public class NewPoint {

    private Integer a;
    private Integer b;

    public NewPoint() {
    }

    public NewPoint(Integer a, Integer b) {
        this.a = a;
        this.b = b;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NewPoint newPoint = (NewPoint) o;

        if (a != null ? !a.equals(newPoint.a) : newPoint.a != null) {
            return false;
        }
        return b != null ? b.equals(newPoint.b) : newPoint.b == null;

    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewPoint{" +
               "a=" + a +
               ", b=" + b +
               '}';
    }
}
