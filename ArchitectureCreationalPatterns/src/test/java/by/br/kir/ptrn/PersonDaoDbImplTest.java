package by.br.kir.ptrn;

import by.br.kir.ptrn.dao.PersonDaoDbImpl;
import by.br.kir.ptrn.model.Person;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kirill-good on 1.3.17.
 */
public class PersonDaoDbImplTest {

    @Test
    public void test() {
        PersonDaoDbImpl dao = new PersonDaoDbImpl("jdbc:hsqldb:mem:testdb", "SA", "");
        dao.writePerson(new Person("123", 123));
        Assert.assertEquals(dao.readPerson("123").getScore(), Integer.valueOf(123));
        dao.writePerson(new Person("1", 123));
        dao.writePerson(new Person("2", 123));
        dao.writePerson(new Person("3", 123));
        dao.writePerson(new Person("4", 123));
        Assert.assertEquals(dao.readPersons().size(), 5);
    }
}
