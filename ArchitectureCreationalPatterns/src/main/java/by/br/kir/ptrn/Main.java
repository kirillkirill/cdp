package by.br.kir.ptrn;

import by.br.kir.ptrn.dao.PersonDao;
import by.br.kir.ptrn.daofactory.PersonDaoFactory;
import by.br.kir.ptrn.daofactory.PersonDaoFactoryImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by kirill-good on 1.3.17.
 */
public class Main {

    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        PersonDaoFactory personDaoFactory = new PersonDaoFactoryImpl();
        PersonDao personDao;
        if (args.length > 0 && args[0].equals("--file")) {
            personDao = personDaoFactory.createFilePersonDao();
        } else {
            personDao = personDaoFactory.createDbPersonDao();
        }
        new Application(personDao).run();
    }
}
