package by.br.kir.ptrn.dao;

import by.br.kir.ptrn.model.Person;

import java.util.List;

/**
 * Created by kirill-good on 1.3.17.
 */
public interface PersonDao {
    void writePerson(Person person);

    List<Person> readPersons();

    Person readPerson(String name);
}