package by.br.kir.ptrn;

import by.br.kir.ptrn.dao.PersonDao;
import by.br.kir.ptrn.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

/**
 * Created by kirill-good on 1.3.17.
 */
public class Application {
    private static final Logger LOG = LogManager.getLogger(Application.class);
    private PersonDao personDao;

    public Application(PersonDao personDao) {
        this.personDao = personDao;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String input;
        LOG.info("Press enter");
        while ((input = scanner.nextLine()) != null) {
            try {
                switch (input) {
                    case "1": {
                        LOG.info("Persons = \n{}", personDao
                                .readPersons()
                                .stream()
                                .map(person -> person.toString() + "\n")
                                .reduce((s1, s2) -> s1 + s2)
                                .get()
                        );
                    }
                    break;
                    case "2": {
                        LOG.info("enter name");
                        String name = scanner.nextLine();
                        LOG.info("Person = {}", personDao.readPerson(name));
                    }
                    break;
                    case "3": {
                        LOG.info("enter name");
                        String name = scanner.nextLine();
                        LOG.info("enter score");
                        Integer score = Integer.valueOf(scanner.nextLine());
                        personDao.writePerson(new Person(name, score));
                    }
                    break;
                }
            } catch (Exception ex) {
                LOG.error(ex);
            }
            LOG.info("\n1 - list\n2 - read person\n3 - write person\n");
        }
    }
}
