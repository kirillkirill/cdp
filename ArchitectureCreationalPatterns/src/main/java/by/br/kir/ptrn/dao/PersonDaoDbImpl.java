package by.br.kir.ptrn.dao;

import by.br.kir.ptrn.model.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirill-good on 1.3.17.
 */
public class PersonDaoDbImpl implements PersonDao {

    public static final String SQL_CREATE_TABLE = "CREATE TABLE persons(person_name varchar(100) UNIQUE , person_score int)";
    public static final String SQL_INSERT = "INSERT into persons (person_name,person_score) VALUES ('%s','%d')";
    public static final String SQL_SELECT_ALL = "SELECT person_name, person_score FROM persons";
    public static final String SQL_SELECT_ONE = "SELECT person_name, person_score FROM persons WHERE person_name='%s'";
    public static final String PERSON_NAME = "person_name";
    public static final String PERSON_SCORE = "person_score";
    private final String dbConnectionString;
    private final String password;
    private final String user;

    public PersonDaoDbImpl(String dbConnectionString, String user, String password) {
        this.dbConnectionString = dbConnectionString;
        this.user = user;
        this.password = password;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            Connection connection = DriverManager.getConnection(dbConnectionString, user, password);
            Statement statement = connection.createStatement();
            statement.execute(SQL_CREATE_TABLE);
            statement.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void writePerson(Person person) {
        try {
            Connection connection = DriverManager.getConnection(dbConnectionString, user, password);
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format(SQL_INSERT, person.getName(), person.getScore()));
            statement.close();
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Person> readPersons() {
        try {
            Connection connection = DriverManager.getConnection(dbConnectionString, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            List<Person> result = new ArrayList<Person>();
            while (resultSet.next()) {
                String personName = resultSet.getString(PERSON_NAME);
                int personScore = resultSet.getInt(PERSON_SCORE);
                Person person = new Person(personName, personScore);
                result.add(person);
            }
            resultSet.close();
            statement.close();
            connection.close();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Person readPerson(String name) {
        try {
            Connection connection = DriverManager.getConnection(dbConnectionString, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(String.format(SQL_SELECT_ONE, name));
            Person person = null;
            while (resultSet.next()) {
                String personName = resultSet.getString(PERSON_NAME);
                int personScore = resultSet.getInt(PERSON_SCORE);
                person = new Person(personName, personScore);
                break;
            }
            resultSet.close();
            statement.close();
            connection.close();
            return person;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
