package by.br.kir.ptrn.dao;

import by.br.kir.ptrn.model.Person;

import java.util.List;

/**
 * Created by kirill-good on 1.3.17.
 */
public class PersonDaoFileImpl implements PersonDao {

    private final String pathToFile;

    public PersonDaoFileImpl(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public void writePerson(Person person) {
        throw new UnsupportedOperationException();
    }

    public List<Person> readPersons() {
        throw new UnsupportedOperationException();
    }

    public Person readPerson(String name) {
        throw new UnsupportedOperationException();
    }
}
