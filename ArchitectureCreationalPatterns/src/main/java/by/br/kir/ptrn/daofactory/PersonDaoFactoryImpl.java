package by.br.kir.ptrn.daofactory;

import by.br.kir.ptrn.dao.PersonDao;
import by.br.kir.ptrn.dao.PersonDaoDbImpl;
import by.br.kir.ptrn.dao.PersonDaoFileImpl;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by kirill-good on 1.3.17.
 */
public class PersonDaoFactoryImpl implements PersonDaoFactory {

    public PersonDao createFilePersonDao() {
        try {
            return new PersonDaoFileImpl(File.createTempFile("cdp", "example").getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public PersonDao createDbPersonDao() {
        return new PersonDaoDbImpl(
                Optional.ofNullable(System.getenv("DB_URL")).orElse("jdbc:hsqldb:mem:testdb"),
                Optional.ofNullable(System.getenv("DB_USER")).orElse("SA"),
                Optional.ofNullable(System.getenv("DB_PASSWORD")).orElse("")
        );
    }
}
