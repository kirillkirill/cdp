package by.br.kir.ptrn.daofactory;

import by.br.kir.ptrn.dao.PersonDao;

/**
 * Created by kirill-good on 1.3.17.
 */
public interface PersonDaoFactory {
    PersonDao createFilePersonDao();

    PersonDao createDbPersonDao();
}
