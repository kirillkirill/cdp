package by.br.kir;

/**
 * Created by kiryl_c on 6/1/17.
 */
public class Counter {

    private int count = 5;

    public void increment() {
        count++;
    }

    public void decrement() {
        count--;
    }

    public int get() {
        return count;
    }
}
