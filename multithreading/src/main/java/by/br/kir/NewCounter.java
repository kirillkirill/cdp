package by.br.kir;

/**
 * Created by kiryl_c on 6/1/17.
 */
public class NewCounter extends Counter {

    private Object lock = new Object();

    @Override
    public void increment() {

        synchronized (lock) {
            lock.notify();
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            super.increment();
        }
    }

    @Override
    public void decrement() {

        synchronized (lock) {
            lock.notify();
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            super.decrement();
        }
    }

    @Override
    public int get() {

        return super.get();
    }
}
