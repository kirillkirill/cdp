package by.br.kir;

import java.util.Random;

/**
 * Created by kiryl_c on 6/1/17.
 */
public class Wrestler implements Runnable {

    private Counter counter;
    private boolean increment;
    private Random rand;
    private String result;

    public Wrestler(Counter counter, boolean increment) {
        this.counter = counter;
        this.increment = increment;
        rand = new Random();
    }

    @Override
    public void run() {
        while (true) {
            if (increment) {
                counter.increment();
            } else {
                counter.decrement();
            }

            int x = counter.get();
            if (x < 0) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException("We have below zero!");
            }

            System.out.println("Wrestler" + Thread.currentThread().getName() + " " + x);
            try {
                Thread.sleep(rand.nextInt(50));
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public String getResult() {
        return result;
    }
}
