package by.br.kir;

/**
 * Created by kiryl_c on 6/1/17.
 */
public class Opposition {

    private Thread t1, t2;

    public void start(Counter counter) {
        t1 = new Thread(new Wrestler(counter, true));
        t2 = new Thread(new Wrestler(counter, false));
        t1.start();
        t2.start();
        try {
            while (true) {
                Thread.sleep(100);
                if (!(t1.isAlive() && t2.isAlive())) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            new RuntimeException(e);
        }
        System.out.println("Finished");
    }

    public void stop() {
        t1.interrupt();
        t2.interrupt();
    }
}