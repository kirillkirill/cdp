package by.br.kir;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kiryl_c on 6/1/17.
 */
public class OppositionTest {

    @Test(timeout = 60000)
    public void testConcurrency() {
        new Opposition().start(new Counter());
    }

    @Test
    public void testConcurrencyWithLock() throws InterruptedException {
        Opposition opposition = new Opposition();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                opposition.start(new NewCounter());
            }
        });
        thread.start();
        Thread.sleep(60000);
        Assert.assertTrue(thread.isAlive());
        opposition.stop();
    }
}
