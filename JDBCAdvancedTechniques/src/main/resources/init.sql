CREATE TABLE user_table(
  user_id BIGINT PRIMARY KEY IDENTITY
  ,user_name VARCHAR(255)
  ,user_sname VARCHAR(255)
  ,user_birth_day DATE
);

CREATE TABLE friendship_table(
  friendship_user1_id BIGINT
  ,friendship_user2_id BIGINT
  ,friendship_timestamp TIMESTAMP
  , PRIMARY KEY (friendship_user1_id,friendship_user2_id)
  , FOREIGN KEY (friendship_user1_id) REFERENCES user_table(user_id)
  , FOREIGN KEY (friendship_user2_id) REFERENCES user_table(user_id)
);

CREATE TABLE post_table(
  post_id BIGINT PRIMARY KEY IDENTITY
  ,post_user_id BIGINT
  ,post_text VARCHAR(255)
  ,post_timestamp TIMESTAMP
  ,FOREIGN KEY (post_user_id) REFERENCES user_table(user_id)
)

CREATE TABLE like_table(
  like_post_id BIGINT
  ,like_user_id BIGINT
  ,like_timestamp TIMESTAMP
  , PRIMARY KEY (like_post_id,like_user_id)
  , FOREIGN KEY (like_post_id) REFERENCES post_table(post_id)
  , FOREIGN KEY (like_user_id) REFERENCES user_table(user_id)
)