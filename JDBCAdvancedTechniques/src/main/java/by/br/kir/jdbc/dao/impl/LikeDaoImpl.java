package by.br.kir.jdbc.dao.impl;

import by.br.kir.jdbc.dao.LikeDao;
import by.br.kir.jdbc.model.Like;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class LikeDaoImpl implements LikeDao {

    public static final String SELECT_FROM_LIKE_TABLE = "SELECT * FROM like_table";
    public static final String
        INSERT_INTO_LIKE_TABLE_IF_NOT_PRESENT =
        "INSERT INTO like_table (like_post_id,like_user_id,like_timestamp)";
    private final Logger LOG = LoggerFactory.getLogger(LikeDaoImpl.class);

    private JdbcTemplate template;

    public LikeDaoImpl(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public void addLikes(List<Like> likes) {
        LOG.debug("addLikes(likes), likes.size() = {}", likes.size());
        template.batchUpdate(INSERT_INTO_LIKE_TABLE_IF_NOT_PRESENT
                             + " (SELECT * FROM (VALUES(?,?,?)) WHERE NOT EXISTS (SELECT * FROM like_table WHERE like_post_id=? AND like_user_id=?))",
                             new BatchPreparedStatementSetter() {
                                 @Override
                                 public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                                     preparedStatement.setLong(1, likes.get(i).getPostId());
                                     preparedStatement.setLong(2, likes.get(i).getUserId());
                                     preparedStatement.setTimestamp(3, likes.get(i).getTimestamp());
                                     preparedStatement.setLong(4, likes.get(i).getPostId());
                                     preparedStatement.setLong(5, likes.get(i).getUserId());
                                 }

                                 @Override
                                 public int getBatchSize() {
                                     return likes.size();
                                 }
                             });
    }

    @Override
    public List<Like> getLikes() {
        List<Like> likes = template.query(SELECT_FROM_LIKE_TABLE, new RowMapper<Like>() {
            @Override
            public Like mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Like(
                    resultSet.getLong(1),
                    resultSet.getLong(2),
                    resultSet.getTimestamp(3)
                );
            }
        });
        LOG.debug("getLikes() = {}", likes);
        return likes;
    }
}
