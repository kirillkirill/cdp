package by.br.kir.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "timestamp")
public class Friendship {

    private Long userId1;
    private Long userId2;
    private Timestamp timestamp;
}
