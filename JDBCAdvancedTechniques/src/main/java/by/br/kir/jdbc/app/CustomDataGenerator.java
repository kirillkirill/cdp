package by.br.kir.jdbc.app;

import by.br.kir.jdbc.dao.FriendshipDao;
import by.br.kir.jdbc.dao.LikeDao;
import by.br.kir.jdbc.dao.PostDao;
import by.br.kir.jdbc.dao.UserDao;
import by.br.kir.jdbc.model.Friendship;
import by.br.kir.jdbc.model.Like;
import by.br.kir.jdbc.model.Post;
import by.br.kir.jdbc.model.User;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
public class CustomDataGenerator {

    private Random random = new Random();
    private Long usersCount;
    private Long postsCount;
    private Long friendshipsCount;
    private Long likesCount;
    private Integer year;
    private UserDao userDao;
    private PostDao postDao;
    private FriendshipDao friendshipDao;
    private LikeDao likeDao;
    private List<String> firstNamesList;
    private List<String> lastNamesList;

    public CustomDataGenerator(Long usersCount, Long postsCount, Long friendshipsCount,
                               Long likesCount, Integer year, UserDao userDao, PostDao postDao,
                               FriendshipDao friendshipDao, LikeDao likeDao,
                               List<String> firstNamesList, List<String> lastNamesList) {
        this.usersCount = usersCount;
        this.postsCount = postsCount;
        this.friendshipsCount = friendshipsCount;
        this.likesCount = likesCount;
        this.year = year;
        this.userDao = userDao;
        this.postDao = postDao;
        this.friendshipDao = friendshipDao;
        this.likeDao = likeDao;
        this.firstNamesList = firstNamesList;
        this.lastNamesList = lastNamesList;
    }

    public void generateDataInDb() {

        userDao.addUsers(generateUsers());
        List<User> users = userDao.getUsers();
        postDao.addPosts(generatePosts(users));
        List<Post> posts = postDao.getPosts();
        friendshipDao.addFriendships(generateFriendships(users));
        likeDao.addLikes(generateLikes(users, posts));
    }

    private List<Like> generateLikes(List<User> users, List<Post> posts) {
        return LongStream
            .range(0, likesCount)
            .mapToObj(value -> new Like(
                getRandomElement(posts).getPostId(),
                getRandomElement(users).getUserId(),
                generateRandomTimestampInYear()
            ))
            .collect(Collectors.toList());
    }

    private Timestamp generateRandomTimestampInYear() {
        long a = Date.valueOf(LocalDate.of(year, 1, 1)).getTime();
        long b = Date.valueOf(LocalDate.of(year, 12, 31)).getTime();
        return new Timestamp(new Date(a + random.nextLong() % (b - a)).getTime());
    }

    private List<Friendship> generateFriendships(List<User> users) {
        return LongStream
            .range(0, friendshipsCount)
            .mapToObj(value -> new Friendship(
                getRandomElement(users).getUserId(),
                getRandomElement(users).getUserId(),
                generateRandomTimestampInYear()
            ))
            .collect(Collectors.toList());
    }

    private List<Post> generatePosts(List<User> users) {
        return LongStream
            .range(0, postsCount)
            .mapToObj(value -> new Post(
                null,
                getRandomElement(users).getUserId(),
                UUID.randomUUID().toString(),
                generateRandomTimestampInYear()
            ))
            .collect(Collectors.toList());
    }

    private <T> T getRandomElement(List<T> list) {
        return list.get(random.nextInt(list.size() - 1));
    }

    private List<User> generateUsers() {

        return LongStream
            .range(0, usersCount)
            .mapToObj(operand -> new User(
                null,
                getRandomName(),
                getRandomSurname(),
                getRandomBirthday()
            ))
            .collect(Collectors.toList());
    }

    private Date getRandomBirthday() {
        long a = Date.valueOf(LocalDate.of(1950, 1, 1)).getTime();
        long b = Date.valueOf(LocalDate.of(2017, 1, 1)).getTime();
        return new Date(a + random.nextLong() % (b - a));
    }

    private String getRandomSurname() {
        return lastNamesList.get(random.nextInt(lastNamesList.size() - 1));
    }

    private String getRandomName() {
        return firstNamesList.get(random.nextInt(firstNamesList.size() - 1));
    }


}
