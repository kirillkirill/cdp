package by.br.kir.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;


/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Post {

    private Long postId;
    private Long userId;
    private String postText;
    private Timestamp timestamp;
}
