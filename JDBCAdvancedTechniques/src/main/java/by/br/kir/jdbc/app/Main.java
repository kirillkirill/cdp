package by.br.kir.jdbc.app;

import by.br.kir.jdbc.dao.UserDao;
import by.br.kir.jdbc.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class Main {

    private final static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        CustomDataGenerator customDataGenerator = context.getBean("customDataGenerator", CustomDataGenerator.class);
        customDataGenerator.generateDataInDb();
        UserDao userDao = context.getBean("userDao", UserDao.class);
        Map<String, String> queryParams = context.getBean("query-params", Map.class);
        List<User> users = userDao.getUsers(
                Integer.valueOf(queryParams.get("minFriendsCount")),
                Integer.valueOf(queryParams.get("minLikes")),
                Date.valueOf(queryParams.get("startDate")),
                Date.valueOf(queryParams.get("endDate"))
        );
        LOG.info("users.size() = {}", users.size());
        users.forEach(user -> {
            LOG.info("result user = {}", user);
        });
    }
}
