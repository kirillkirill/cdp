package by.br.kir.jdbc.dao.impl;

import by.br.kir.jdbc.dao.UserDao;
import by.br.kir.jdbc.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.*;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class UserDaoImpl implements UserDao {

    @Value("#{T(org.apache.commons.io.IOUtils).toString((new org.springframework.core.io.ClassPathResource('sql/select_from_user_table.sql')).getInputStream())}")
    public String selectFromUserTable;
    @Value("#{T(org.apache.commons.io.IOUtils).toString((new org.springframework.core.io.ClassPathResource('sql/insert_into_user_table.sql')).getInputStream())}")
    public String insertIntoUserTable;
    @Value("#{T(org.apache.commons.io.IOUtils).toString((new org.springframework.core.io.ClassPathResource('sql/select_user_by_criteria.sql')).getInputStream())}")
    public String sqlSelectUserByCriteria;

    private final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class);

    private JdbcTemplate template;

    public UserDaoImpl(JdbcTemplate template) {
        this.template = template;
    }


    @Override
    public List<User> getUsers(int minFriendsCount, int minLikes, Date startDate, Date endDate) {
        LOG.debug("getUsers(minFriendsCount={} minLikes={} startDate={} endDate={})", minFriendsCount, minLikes,
                startDate, endDate);
        List<User> users = template.query(
                sqlSelectUserByCriteria,
                new UserRowMapper(),
                minFriendsCount,
                new Timestamp(startDate.getTime()),
                new Timestamp(endDate.getTime()),
                minLikes
        );
        LOG.debug("getUsers(minFriendsCount={} minLikes={} startDate={} endDate={})={}", minFriendsCount, minLikes,
                startDate, endDate, users);
        return users;
    }

    @Override
    public List<User> getUsers() {
        List<User> users = template.query(selectFromUserTable, new UserRowMapper());
        LOG.debug("getUsers() = {}", users);
        return users;
    }

    @Override
    public void addUsers(List<User> users) {
        LOG.debug("addUsers(users), users.size() = {}", users.size());
        template.batchUpdate(insertIntoUserTable,
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                        preparedStatement.setString(1, users.get(i).getUserName());
                        preparedStatement.setString(2, users.get(i).getUserSurname());
                        preparedStatement.setDate(3, users.get(i).getUserBirthdate());
                    }

                    @Override
                    public int getBatchSize() {
                        return users.size();
                    }
                });
    }

    static class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            return new User(
                    resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getDate(4)
            );
        }
    }
}
