package by.br.kir.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class User {

    private Long userId;
    private String userName;
    private String userSurname;
    private Date userBirthdate;
}
