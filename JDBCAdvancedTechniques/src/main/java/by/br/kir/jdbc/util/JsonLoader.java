package by.br.kir.jdbc.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class JsonLoader<T> {

    private String fileName;
    private Class<T> type;

    public JsonLoader(String fileName, Class<T> type) {
        this.fileName = fileName;
        this.type = type;
    }

    public T loadObjectList() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new ClassPathResource(fileName).getInputStream(), type);
    }
}
