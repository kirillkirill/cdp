package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Post;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
public interface PostDao {

    void addPosts(List<Post> posts);

    List<Post> getPosts();
}
