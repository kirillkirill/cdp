package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Friendship;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
public interface FriendshipDao {

    void addFriendships(List<Friendship> friendships);

    List<Friendship> getFriendships();
}
