package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Like;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
public interface LikeDao {

    void addLikes(List<Like> likes);

    List<Like> getLikes();
}
