package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.User;

import java.sql.Date;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
public interface UserDao {

    List<User> getUsers(int minFriendsCount, int minLikes, Date startDate, Date endDate);

    List<User> getUsers();

    void addUsers(List<User> users);
}
