package by.br.kir.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * Created by kiryl_chepeleu on 3/29/17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "timestamp")
public class Like {

    private Long postId;
    private Long userId;
    private Timestamp timestamp;
}
