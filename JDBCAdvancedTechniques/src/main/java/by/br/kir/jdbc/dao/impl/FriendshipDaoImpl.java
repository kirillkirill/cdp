package by.br.kir.jdbc.dao.impl;

import by.br.kir.jdbc.dao.FriendshipDao;
import by.br.kir.jdbc.model.Friendship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class FriendshipDaoImpl implements FriendshipDao {

    public static final String SELECT_FROM_FRIENDSHIP_TABLE = "SELECT * FROM friendship_table";
    public static final String
        INSERT_INTO_FRIENDSHIP_TABLE_IF_NOT_PRESENT =
        "INSERT INTO friendship_table (friendship_user1_id,friendship_user2_id,friendship_timestamp) "
        + "SELECT * FROM (VALUES (?,?,?)) WHERE NOT EXISTS (SELECT * FROM friendship_table "
        + "WHERE friendship_user1_id=? AND friendship_user2_id=?)";
    private final Logger LOG = LoggerFactory.getLogger(FriendshipDaoImpl.class);

    private JdbcTemplate template;

    public FriendshipDaoImpl(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public void addFriendships(List<Friendship> friendships) {
        LOG.debug("addFriendships(friendships), friendships.size() = {}", friendships.size());
        template.batchUpdate(
            INSERT_INTO_FRIENDSHIP_TABLE_IF_NOT_PRESENT,
            new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                    Long
                        userId1 =
                        Long.min(friendships.get(i).getUserId1(), friendships.get(i).getUserId2());
                    Long
                        userId2 =
                        Long.max(friendships.get(i).getUserId1(), friendships.get(i).getUserId2());
                    preparedStatement.setLong(1, userId1);
                    preparedStatement.setLong(2, userId2);
                    preparedStatement.setTimestamp(3, friendships.get(i).getTimestamp());
                    preparedStatement.setLong(4, userId1);
                    preparedStatement.setLong(5, userId2);
                }

                @Override
                public int getBatchSize() {
                    return friendships.size();
                }
            });
    }

    @Override
    public List<Friendship> getFriendships() {
        List<Friendship> friendships = template.query(SELECT_FROM_FRIENDSHIP_TABLE, new RowMapper<Friendship>() {
            @Override
            public Friendship mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Friendship(
                    resultSet.getLong(1),
                    resultSet.getLong(2),
                    resultSet.getTimestamp(3)
                );
            }
        });
        LOG.debug("getFriendships() = {}", friendships);
        return friendships;
    }
}
