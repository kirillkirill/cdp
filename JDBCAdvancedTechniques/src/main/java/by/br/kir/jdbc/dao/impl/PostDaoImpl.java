package by.br.kir.jdbc.dao.impl;

import by.br.kir.jdbc.dao.PostDao;
import by.br.kir.jdbc.model.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class PostDaoImpl implements PostDao {

    public static final String
        INSERT_INTO_POST_TABLE =
        "INSERT INTO post_table (post_user_id,post_text,post_timestamp) VALUES (?,?,?)";
    public static final String SELECT_FROM_POST_TABLE = "SELECT * FROM post_table";
    private final Logger LOG = LoggerFactory.getLogger(PostDaoImpl.class);
    private JdbcTemplate template;

    public PostDaoImpl(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public void addPosts(List<Post> posts) {
        LOG.debug("addPosts(posts), posts.size() = {}", posts.size());
        template.batchUpdate(INSERT_INTO_POST_TABLE,
                             new BatchPreparedStatementSetter() {
                                 @Override
                                 public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                                     preparedStatement.setLong(1, posts.get(i).getUserId());
                                     preparedStatement.setString(2, posts.get(i).getPostText());
                                     preparedStatement.setTimestamp(3, posts.get(i).getTimestamp());
                                 }

                                 @Override
                                 public int getBatchSize() {
                                     return posts.size();
                                 }
                             });
    }

    @Override
    public List<Post> getPosts() {
        List<Post> posts = template.query(SELECT_FROM_POST_TABLE, new RowMapper<Post>() {
            @Override
            public Post mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Post(
                    resultSet.getLong(1),
                    resultSet.getLong(2),
                    resultSet.getString(3),
                    resultSet.getTimestamp(4)
                );
            }
        });
        LOG.debug("getPosts() = {}", posts);
        return posts;
    }
}
