package by.br.kir.jdbc.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/context.xml",
    "classpath:/test-context.xml"
})
public class DaoTest {

    @Autowired
    private UserDaoTest userDaoTest;
    @Autowired
    private LikeDaoTest likeDaoTest;
    @Autowired
    private PostDaoTest postDaoTest;
    @Autowired
    private FriendshipDaoTest friendshipDaoTest;

    @Test
    public void test1() {
        userDaoTest.testAddUsersAndGetUsers();
        friendshipDaoTest.testAddFriendshipsAndGetFriendShips();
        postDaoTest.testAddPostsAndGetPosts();
        likeDaoTest.testAddLikesAndGetLikes();
        userDaoTest.testGetUsersByStatement();
    }
}
