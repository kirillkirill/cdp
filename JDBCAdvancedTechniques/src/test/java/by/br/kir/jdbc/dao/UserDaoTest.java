package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Friendship;
import by.br.kir.jdbc.model.Like;
import by.br.kir.jdbc.model.Post;
import by.br.kir.jdbc.model.User;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class UserDaoTest {

    private final static Logger LOG = LoggerFactory.getLogger(UserDaoTest.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private FriendshipDao friendshipDao;
    @Autowired
    private PostDao postDao;
    @Autowired
    private LikeDao likeDao;

    public void testAddUsersAndGetUsers() {
        List<User> users = Arrays.asList(
            new User(null, "name1", "sname1", Date.valueOf(LocalDate.now())),
            new User(null, "name2", "sname2", Date.valueOf(LocalDate.now())),
            new User(null, "name3", "sname3", Date.valueOf(LocalDate.now())),
            new User(null, "name4", "sname4", Date.valueOf(LocalDate.now()))
        );
        userDao.addUsers(users);
        List<User> usersFromDb = userDao.getUsers();
        Assert.assertEquals(users.size(), usersFromDb.size());
        for (int i = 0; i < users.size(); i++) {
            Assert.assertEquals(users.get(i).getUserName(), usersFromDb.get(i).getUserName());
            Assert.assertEquals(users.get(i).getUserSurname(), usersFromDb.get(i).getUserSurname());
            Assert.assertEquals(users.get(i).getUserBirthdate(), usersFromDb.get(i).getUserBirthdate());
            LOG.info("usersFromDb = {}", usersFromDb.get(i));
        }
    }

    public void testGetUsersByStatement() {
        Date startDate = Date.valueOf(LocalDate.of(2017, 1, 1));
        Date endDate = Date.valueOf(LocalDate.of(2017, 12, 31));
        int minFriendsCount = 2;
        int minLikes = 10;
        List<User> usersByStatement = userDao.getUsers(minFriendsCount, minLikes, startDate, endDate);
        List<User> allUsersFromDb = userDao.getUsers();
        List<Friendship> allFriendshipsFromDb = friendshipDao.getFriendships();
        List<Post> allPostsFromDb = postDao.getPosts();
        List<Like> allLikesFromDb = likeDao.getLikes();
        List<User> usersToCompare = allUsersFromDb
            .stream()
            .filter(user -> {
                long friendsCount = allFriendshipsFromDb
                    .stream()
                    .filter(friendship -> friendship.getUserId1() == user.getUserId() || friendship.getUserId2() == user
                        .getUserId())
                    .count();

                long likesCount = allPostsFromDb
                    .stream()
                    .filter(post -> post.getUserId() == user.getUserId())
                    .flatMap(post -> allLikesFromDb
                        .stream()
                        .filter(like -> like.getPostId() == post.getPostId()
                                        && like.getTimestamp().getTime() < endDate.getTime()
                                        && like.getTimestamp().getTime() > startDate.getTime())
                        .collect(Collectors.toList())
                        .stream()
                    ).count();
                return likesCount > minLikes && friendsCount > minFriendsCount;
            })
            .collect(Collectors.toList());

        Assert.assertEquals(usersToCompare.size(), usersByStatement.size());
    }
}
