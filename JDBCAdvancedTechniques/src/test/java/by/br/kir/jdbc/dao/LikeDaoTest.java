package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Like;
import by.br.kir.jdbc.model.Post;
import by.br.kir.jdbc.model.User;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class LikeDaoTest {

    private final Logger LOG = LoggerFactory.getLogger(LikeDaoTest.class);

    @Autowired
    private LikeDao likeDao;
    @Autowired
    private PostDao postDao;
    @Autowired
    private UserDao userDao;

    public void testAddLikesAndGetLikes() {
        List<Post> posts = postDao.getPosts();
        List<User> users = userDao.getUsers();
        final int n = 1000;
        Random random = new Random();
        List<Like> likes = IntStream
            .range(0, n)
            .mapToObj(v -> new Like(
                posts.get(random.nextInt(posts.size() - 1)).getPostId(),
                users.get(random.nextInt(users.size() - 1)).getUserId(),
                new Timestamp(new Date().getTime())
            ))
            .collect(Collectors.toSet())
            .stream()
            .collect(Collectors.toList());
        likeDao.addLikes(likes);
        List<Like> likesFromDb = likeDao.getLikes();
        Assert.assertEquals(likes.size(), likesFromDb.size());
        for (int i = 0; i < likes.size(); i++) {
            LOG.debug("likes(i)={}, \nlikesFromDb={}", likes.get(i), likesFromDb);
            Assert.assertTrue(likesFromDb.contains(likes.get(i)));
        }
    }
}
