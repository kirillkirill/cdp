package by.br.kir.jdbc.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/context.xml",
    "classpath:/test-context.xml"
})
public class JsonLoaderTest {

    private final static Logger LOG = LoggerFactory.getLogger(JsonLoaderTest.class);

    @Autowired
    JsonLoader<List> jsonFirstNamesLoader;

    @Autowired
    JsonLoader<List> jsonLastNamesLoader;

    @Test
    public void jsonLoaderTest() throws IOException {

        LOG.info("jsonLoaderTest");
        List firstNamesList = jsonFirstNamesLoader.loadObjectList();
        Assert.assertEquals(4946, firstNamesList.size());

        List lastNamesList = jsonLastNamesLoader.loadObjectList();
        Assert.assertEquals(21986, lastNamesList.size());
    }
}
