package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Friendship;
import by.br.kir.jdbc.model.User;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class FriendshipDaoTest {

    @Autowired
    private FriendshipDao friendshipDao;
    @Autowired
    private UserDao userDao;

    public void testAddFriendshipsAndGetFriendShips() {
        List<User> users = userDao.getUsers();
        final int n = 15;
        Random random = new Random();
        Set<Friendship> friendships = IntStream
            .range(0, n)
            .mapToObj(v -> new Friendship(
                users.get(random.nextInt(users.size())).getUserId(),
                users.get(random.nextInt(users.size())).getUserId(),
                new Timestamp(new Date().getTime())
            ))
            .collect(Collectors.toSet());

        friendshipDao.addFriendships(new ArrayList<>(friendships));
        friendships = friendships
            .stream()
            .peek(friendship -> {
                Long userId1 = friendship.getUserId1();
                Long userId2 = friendship.getUserId2();
                friendship.setUserId1(Long.min(userId1, userId2));
                friendship.setUserId2(Long.max(userId1, userId2));
            }).collect(Collectors.toSet());
        List<Friendship> friendshipsFromDb = friendshipDao.getFriendships();
        Assert.assertEquals(friendships.size(), friendshipsFromDb.size());
        for (int i = 0; i < friendshipsFromDb.size(); i++) {
            Assert.assertTrue(friendships.contains(friendshipsFromDb.get(i)));
        }
    }
}
