package by.br.kir.jdbc.dao;

import by.br.kir.jdbc.model.Post;
import by.br.kir.jdbc.model.User;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kiryl_chepeleu on 3/30/17.
 */
public class PostDaoTest {

    private final static Logger LOG = LoggerFactory.getLogger(UserDaoTest.class);

    @Autowired
    private PostDao postDao;
    @Autowired
    private UserDao userDao;

    public void testAddPostsAndGetPosts() {
        List<User> users = userDao.getUsers();
        final int n = 100;
        Random random = new Random();
        List<Post> posts = IntStream
            .range(0, n)
            .mapToObj(value -> new Post(
                null,
                users.get(random.nextInt(users.size() - 1)).getUserId(),
                UUID.randomUUID().toString(),
                new Timestamp(new Date().getTime())
            ))
            .collect(Collectors.toList());
        postDao.addPosts(posts);
        List<Post> postsFromDb = postDao.getPosts();
        Assert.assertEquals(posts.size(), postsFromDb.size());
        for (int i = 0; i < posts.size(); i++) {
            Assert.assertEquals(posts.get(i).getUserId(), postsFromDb.get(i).getUserId());
            Assert.assertEquals(posts.get(i).getPostText(), postsFromDb.get(i).getPostText());
            Assert.assertEquals(posts.get(i).getTimestamp(), postsFromDb.get(i).getTimestamp());
        }
    }
}
