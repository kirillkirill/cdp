Infrastructure: Tomcat+ Apache default task

Configure the Tomcat and Apache integration with mod_jk.so module. 

Build multi-module web application and deploy with tomcat manager application (text/script mode). 

Static (html, css, js) publish to apache, dynamic to tomcat. 

Test and write readme, how mentor can deploy it and check that it is working.


To run :


    git clone https://bitbucket.org/kirillkirill/cdp.git

    cd cdp/fileShare/

    mvn clean install

    sudo bash ./create_docker_image.sh

    sudo bash ./run_docker_image.sh

Now fileShare app is available on http://localhost:8080/fileShare/ by tomcat, and on http://localhost/fileShare/ by apache2