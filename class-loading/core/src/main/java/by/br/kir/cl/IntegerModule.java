package by.br.kir.cl;

/**
 * Created by kiryl_chepeleu on 2/23/17.
 */
public interface IntegerModule {

    Integer process(Integer... objects);
}
