package by.br.kir.cl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by kiryl_chepeleu on 2/23/17.
 */
public class AddOperationIntegerModule implements IntegerModule {

    private static final Logger LOG = LogManager.getLogger(AddOperationIntegerModule.class);

    static {
        LOG.debug("AddOperationIntegerModule static init");
    }

    {
        LOG.debug("AddOperationIntegerModule instance init");
    }

    public Integer process(Integer... objects) {

        LOG.debug("Input integers = {}", Arrays.toString(objects));
        return Stream
            .of(objects)
            .reduce((i1, i2) -> i1 + i2)
            .get();
    }
}
