package by.br.kir.cl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by kiryl_chepeleu on 2/23/17.
 */
public class CustomClassLoader extends ClassLoader {

    private static final Logger LOG = LogManager.getLogger(CustomClassLoader.class);

    private final ClassLoader parent;
    private final String jarFile;

    public CustomClassLoader(ClassLoader parent, String jarFile) {

        super(parent);
        this.parent = parent;
        this.jarFile = jarFile;
    }

    @Override
    public Class<?> findClass(String className) throws ClassNotFoundException {

        LOG.debug("CustomClassLoader findClass, className={}", className);
        try {
            byte b[] = ZipUtils.readFileFromZip(classNameToPath(className), jarFile);
            return defineClass(className, b, 0, b.length);
        } catch (IOException ex) {
            LOG.debug("IOException, delegate to parent", ex);
            return super.findClass(className);
        }
    }


    private String classNameToPath(String className) {
        return className.replaceAll("[.]", "/") + ".class";
    }
}
