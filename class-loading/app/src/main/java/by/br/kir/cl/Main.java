package by.br.kir.cl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by kirill-good on 22.2.17.
 */
public class Main {

    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        LOG.info("Starting app");
        new Application().run();
    }
}

