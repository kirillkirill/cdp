package by.br.kir.cl;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by kiryl_chepeleu on 2/23/17.
 */
public class ZipUtils {

    private static final Logger LOG = LogManager.getLogger(ZipUtils.class);

    public static byte[] readFileFromZip(String fileToBeExtracted, String zipPackage) throws IOException {

        LOG.debug("readFileFromZip fileTobeExtracted={} zipPackage={}", fileToBeExtracted, zipPackage);
        FileInputStream fileInputStream = new FileInputStream(zipPackage);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        try (ZipInputStream zin = new ZipInputStream(bufferedInputStream)) {
            ZipEntry ze;
            while ((ze = zin.getNextEntry()) != null) {
                LOG.debug("readFileFromZip, founded: ZipEntry={}", ze);
                if (ze.getName().equals(fileToBeExtracted)) {
                    byte[] bytes = IOUtils.toByteArray(zin);
                    zin.close();
                    return bytes;
                }
            }
            LOG.error("readFileFromZip, file not found fileTobeExtracted={}", fileToBeExtracted);
            throw new FileNotFoundException(fileToBeExtracted);
        }
    }
}
