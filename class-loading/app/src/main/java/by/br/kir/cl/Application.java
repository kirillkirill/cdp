package by.br.kir.cl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by kiryl_chepeleu on 2/23/17.
 */
public class Application {

    private static final Logger LOG = LogManager.getLogger(Application.class);

    private IntegerModule module;
    private Scanner scanner = new Scanner(System.in);

    public void run() {

        String line;
        LOG.info("Press enter");

        while ((line = scanner.nextLine()) != null) {
            try {
                switch (line) {
                    case "1":
                        loadModule();
                        break;
                    case "2":
                        callModule();
                        break;
                }
            } catch (Exception ex) {
                LOG.error(ex);
            }
            LOG.info("\n1 - load module\n2 - call module\n");
        }
    }

    private void callModule() {

        if (module == null) {
            LOG.info("Load module please");
        } else {
            LOG.info("Type input value");
            String value = scanner.nextLine();
            List<Integer> collect = Stream
                .of(value.split(" "))
                .filter(s -> !s.isEmpty())
                .map(s -> Integer.valueOf(s))
                .collect(Collectors.toList());
            Integer[] integers = collect.toArray(new Integer[collect.size()]);
            Integer process = module.process(integers);
            LOG.info("result = {}", process);
        }
    }

    private void loadModule() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        LOG.info("Type jar file");
        String jarFile = scanner.nextLine();
        LOG.info("Type full class name");
        String className = scanner.nextLine();
        CustomClassLoader customClassLoader = new CustomClassLoader(
            ClassLoader.getSystemClassLoader(), jarFile
        );
        Class<IntegerModule> moduleClass = (Class<IntegerModule>) Class.forName(className, true, customClassLoader);
        module = moduleClass.newInstance();
    }
}
