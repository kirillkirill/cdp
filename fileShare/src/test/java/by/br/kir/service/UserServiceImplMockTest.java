package by.br.kir.service;

import by.br.kir.dao.UserDao;
import by.br.kir.model.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Created by kiryl_c on 5/26/17.
 */
public class UserServiceImplMockTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void save() {
        User user = new User();
        userService.createUser(user);
        Mockito.verify(userDao).createUser(user);
    }

    @Test
    public void get() {
        String user = "user";
        userService.getUserByLogin(user);
        Mockito.verify(userDao).getUserByLogin(user);
    }
}
