package by.br.kir.service;

import by.br.kir.dao.FileDao;
import by.br.kir.model.FileEntity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Created by kiryl_c on 5/26/17.
 */
public class FileServiceMockTest {

    @Mock
    private FileDao fileDao;

    @InjectMocks
    private SimpleFileServiceImpl simpleFileService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void save() {
        FileEntity fileEntity = new FileEntity();
        simpleFileService.saveFile(fileEntity);
        Mockito.verify(fileDao).saveFileEntity(fileEntity);
    }

    @Test
    public void remove() {
        simpleFileService.removeFileEntityById(1L);
        Mockito.verify(fileDao).removeFileEntityById(1L);
    }

    @Test
    public void getAll() {
        simpleFileService.getFileEntities();
        Mockito.verify(fileDao).getFileEntities();
    }

    @Test
    public void getById() {
        simpleFileService.getFileEntityById(1L);
        Mockito.verify(fileDao).getFileEntityById(1L);
    }
}
