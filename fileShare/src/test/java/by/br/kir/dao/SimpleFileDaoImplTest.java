package by.br.kir.dao;

import by.br.kir.config.MvcConfiguration;
import by.br.kir.model.FileEntity;
import by.br.kir.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kiryl_chepeleu on 4/12/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MvcConfiguration.class})
@WebAppConfiguration
public class SimpleFileDaoImplTest {

    @Autowired
    private UserDao userDao;
    @Autowired
    private FileDao fileDao;

    @Test(timeout = 10000)
    public void testDao() throws SQLException {
        User user1 = new User(null, "login1", "pass1", "role1");
        User user2 = new User(null, "login2", "pass1", "role1");
        User user3 = new User(null, "login3", "pass1", "role1");
        userDao.createUser(user1);
        userDao.createUser(user2);
        userDao.createUser(user3);
        User userByLogin1 = userDao.getUserByLogin(user1.getLogin());
        User userByLogin2 = userDao.getUserByLogin(user2.getLogin());
        User userByLogin3 = userDao.getUserByLogin(user3.getLogin());
        Assert.assertEquals(user1.getLogin(), userByLogin1.getLogin());
        Assert.assertEquals(user1.getPassword(), userByLogin1.getPassword());
        Assert.assertEquals(user1.getRole(), userByLogin1.getRole());
        Assert.assertEquals(user2.getLogin(), userByLogin2.getLogin());
        Assert.assertEquals(user2.getPassword(), userByLogin2.getPassword());
        Assert.assertEquals(user2.getRole(), userByLogin2.getRole());
        Assert.assertEquals(user3.getLogin(), userByLogin3.getLogin());
        Assert.assertEquals(user3.getPassword(), userByLogin3.getPassword());
        Assert.assertEquals(user3.getRole(), userByLogin3.getRole());

        FileEntity fileEntity1 = new FileEntity(null, "11", null, new byte[]{1}, userByLogin1);
        FileEntity fileEntity2 = new FileEntity(null, "22", null, new byte[]{2}, userByLogin2);
        FileEntity fileEntity3 = new FileEntity(null, "33", null, new byte[]{3}, userByLogin3);
        List<FileEntity> fileEntityListOriginal = Arrays.asList(
            fileEntity1,
            fileEntity2,
            fileEntity3
        );
        fileDao.saveFileEntity(fileEntity1);
        fileDao.saveFileEntity(fileEntity2);
        fileDao.saveFileEntity(fileEntity3);

        List<FileEntity> fileEntities = fileDao.getFileEntities();
        System.out.println("fileEntities = " + fileEntities);
        Assert.assertEquals(3, fileEntities.size());
        fileEntities = fileEntities
            .stream()
            .sorted((o1, o2) -> Long.compare(o1.getTimestamp().getTime(), o2.getTimestamp().getTime()))
            .collect(Collectors.toList());
        for (int i = 0; i < fileEntities.size(); i++) {
            FileEntity fileEntity = fileEntities.get(i);
            FileEntity fileEntityOriginal = fileEntityListOriginal.get(i);
            final Long fileEntityId = fileEntity.getId();
            FileEntity fileEntityById = fileDao.getFileEntityById(fileEntityId);
            Assert.assertEquals(fileEntityOriginal.getOriginalFileName(),
                                fileEntityById.getOriginalFileName());
            Assert.assertTrue(Arrays.equals(fileEntityOriginal.getBody(), fileEntityById.getBody()));
            Assert.assertNotNull(fileEntityById.getTimestamp());
        }
        for (int i = 0; i < fileEntities.size(); i++) {
            Assert.assertEquals(fileEntities.size() - i, fileDao.getFileEntities().size());
            fileDao.removeFileEntityById(fileEntities.get(i).getId());
            Assert.assertEquals(fileEntities.size() - 1 - i, fileDao.getFileEntities().size());
        }
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testWithViolation() throws SQLException {
        userDao.createUser(new User(null, "admin", "admin", "admin"));
    }
}
