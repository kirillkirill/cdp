package by.br.kir.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import by.br.kir.config.MvcConfiguration;
import by.br.kir.model.FileEntity;
import by.br.kir.service.FileService;
import by.br.kir.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kiryl_c on 5/23/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MvcConfiguration.class})
@WebAppConfiguration
public class HomeControllerTest {

    private MockMvc mvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private WebApplicationContext context;

    @Mock
    private FileService fileService;
    @Mock
    private UserService userService;

    @Autowired
    @InjectMocks
    private HomeController homeController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .addFilter(springSecurityFilterChain)
            .build();
    }

    private final FileEntity fileEntity = new FileEntity(13L,
                                                         "13",
                                                         new Timestamp(System.currentTimeMillis()),
                                                         new byte[13],
                                                         new by.br.kir.model.User(
                                                             13L, "13", "13", "13"
                                                         )
    );

    @Test
    public void downloadTest() throws Exception {
        when(fileService.getFileEntityById(13L)).thenReturn(fileEntity);
        MvcResult mvcResult = mvc
            .perform(get("/download/13").with(user("admin").password("pass").roles("USER")))
            .andExpect(status().is2xxSuccessful()).andReturn();
        Assert.assertTrue(mvcResult.getResponse().getHeader("Content-Type").equals("application/octet-stream"));
        Assert.assertArrayEquals(fileEntity.getBody(), mvcResult.getResponse().getContentAsByteArray());
    }

    @Test
    public void uploadTest() throws Exception {
        MultipartFile file = Mockito.mock(MultipartFile.class);
        when(file.getBytes()).thenReturn(new byte[13]);
        when(file.getOriginalFilename()).thenReturn("13.13");
        MvcResult mvcResult = mvc
            .perform(
                fileUpload("/upload").file("file", file.getBytes()).with(user("admin").password("pass").roles("USER"))
            )
            .andExpect(status().is3xxRedirection()).andReturn();

        Mockito.verify(fileService).saveFile(any());
        Mockito.verify(userService).getUserByLogin("admin");
    }

    @Test
    public void testHome() throws Exception {

        mvc
            .perform(get("/"))
            .andExpect(status().is3xxRedirection());

        List<FileEntity> entityList = Arrays.asList(fileEntity);
        when(fileService.getFileEntities()).thenReturn(entityList);
        MvcResult mvcResult = mvc
            .perform(get("/").with(user("admin").password("pass").roles("USER")))
            .andExpect(status().is2xxSuccessful()).andReturn();
        Assert.assertEquals(entityList, mvcResult.getModelAndView().getModel().get("files"));
        Assert.assertEquals(true, mvcResult.getModelAndView().getModel().get("isAuthenticated"));
        Assert.assertEquals(false, mvcResult.getModelAndView().getModel().get("isAdmin"));
        Assert.assertEquals("admin", mvcResult.getModelAndView().getModel().get("userName"));
        Assert.assertEquals("home", mvcResult.getModelAndView().getViewName());

        mvcResult = mvc
            .perform(get("/").with(user("admin").password("pass").roles("ADMIN")))
            .andExpect(status().is2xxSuccessful()).andReturn();

        Assert.assertEquals(entityList, mvcResult.getModelAndView().getModel().get("files"));
        Assert.assertEquals(true, mvcResult.getModelAndView().getModel().get("isAuthenticated"));
        Assert.assertEquals(true, mvcResult.getModelAndView().getModel().get("isAdmin"));
        Assert.assertEquals("admin", mvcResult.getModelAndView().getModel().get("userName"));
        Assert.assertEquals("home", mvcResult.getModelAndView().getViewName());
    }

    @Test
    public void removeTestUserRole() throws Exception {
        MvcResult mvcResult = mvc
            .perform(
                get("/admin/remove/13")
                    .with(user("admin").password("pass").roles("USER"))
            )
            .andExpect(status().is4xxClientError()).andReturn();
    }

    @Test
    public void removeTestAdminRole() throws Exception {
        MvcResult mvcResult = mvc
            .perform(
                get("/admin/remove/13")
                    .with(user("admin").password("pass").roles("ADMIN"))
            )
            .andExpect(status().is3xxRedirection()).andReturn();
        Mockito.verify(fileService).removeFileEntityById(13L);
    }

    @Test
    public void logoutTest() throws Exception {
        MvcResult mvcResult = mvc
            .perform(
                get("/logout")
                    .with(user("admin").password("pass").roles("USER"))
            )
            .andExpect(status().is3xxRedirection()).andReturn();
    }

    @Test
    public void regTest() throws Exception {
        MvcResult mvcResult = mvc
            .perform(
                post("/reg")
                    .param("username", "username")
                    .param("password", "password")
            )
            .andReturn();
        Assert.assertEquals("login", mvcResult.getModelAndView().getViewName());
        Assert.assertEquals("User was created successfully!", mvcResult.getModelAndView().getModel().get("message"));
        Mockito.verify(userService).createUser(any());
    }

    @Test
    public void regTestInvalidUser() throws Exception {
        MvcResult mvcResult = mvc
            .perform(
                post("/reg")
                    .param("username", "u%%")
                    .param("password", "password")
            )
            .andReturn();
        Assert.assertEquals("login", mvcResult.getModelAndView().getViewName());
        Assert.assertEquals(
            "Invalid user login, try only letters, digits and _-., at least 3 chars",
            mvcResult.getModelAndView().getModel().get("message")
        );
    }
}
