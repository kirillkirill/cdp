CREATE TABLE file_table(
  file_id BIGINT PRIMARY KEY IDENTITY
  ,original_file_name VARCHAR(255)
  ,file_timestamp TIMESTAMP DEFAULT current_timestamp
  ,file_body BLOB
  ,file_user_id BIGINT NOT NULL
);

CREATE TABLE user_table(
  user_id BIGINT not null primary key GENERATED ALWAYS AS IDENTITY
  ,user_login VARCHAR(255) UNIQUE
  ,user_password VARCHAR(255)
  ,user_role VARCHAR(255)
);

ALTER TABLE file_table
ADD FOREIGN KEY (file_user_id) REFERENCES user_table(user_id)

INSERT INTO user_table (user_login,user_password,user_role) VALUES ('user','user','ROLE_USER')
INSERT INTO user_table (user_login,user_password,user_role) VALUES ('admin','admin','ROLE_ADMIN')