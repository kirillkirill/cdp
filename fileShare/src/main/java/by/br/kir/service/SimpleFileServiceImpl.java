package by.br.kir.service;

import by.br.kir.dao.FileDao;
import by.br.kir.model.FileEntity;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 4/12/17.
 */
public class SimpleFileServiceImpl implements FileService {

    private FileDao fileDao;

    public SimpleFileServiceImpl(FileDao fileDao) {
        this.fileDao = fileDao;
    }

    @Override
    public void removeFileEntityById(Long fileId) {
        fileDao.removeFileEntityById(fileId);
    }

    @Override
    public void saveFile(FileEntity fileEntity) {
        fileDao.saveFileEntity(fileEntity);
    }

    @Override
    public FileEntity getFileEntityById(Long fileId) {
        return fileDao.getFileEntityById(fileId);
    }

    @Override
    public List<FileEntity> getFileEntities() {
        return fileDao.getFileEntities();
    }
}
