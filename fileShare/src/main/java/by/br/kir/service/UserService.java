package by.br.kir.service;

import by.br.kir.model.User;

/**
 * Created by kirill-good on 8.5.17.
 */
public interface UserService {

    void createUser(User user);

    User getUserByLogin(String login);
}
