package by.br.kir.service;

import by.br.kir.model.FileEntity;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 4/12/17.
 */
public interface FileService {

    void removeFileEntityById(Long fileId);

    void saveFile(FileEntity fileEntity);

    FileEntity getFileEntityById(Long fileId);

    List<FileEntity> getFileEntities();
}
