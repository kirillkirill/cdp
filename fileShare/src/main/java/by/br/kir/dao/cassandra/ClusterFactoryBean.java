package by.br.kir.dao.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by kiryl_c on 6/15/17.
 */
public class ClusterFactoryBean {

    //    @Value("#{T(org.apache.commons.io.IOUtils).toString((new org.springframework.core.io.ClassPathResource('sql/select_from_user_table.sql')).getInputStream())}")
//    public String selectFromUserTable;
    public Cluster buildCluster(String address, Integer port) throws IOException {
        final Cluster cluster = Cluster
            .builder()
            .addContactPoint(address)
            .withPort(port)
            .build();
        final Session session = cluster.connect();
        String[] split = IOUtils.toString(new ClassPathResource("init.cqlsh").getInputStream()).split(";");
        Arrays
            .stream(split)
            .forEach(s -> {
                System.out.println("s = " + s);
                System.out.println("s.trim() = " + s.trim());
                System.out.println("s.trim() + \";\" = " + s.trim() + ";");
                session.execute(s.trim() + ";");
            });
        return cluster;
    }
}
