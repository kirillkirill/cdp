package by.br.kir.dao.cassandra;

import by.br.kir.dao.FileDao;
import by.br.kir.model.FileEntity;
import by.br.kir.model.User;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by kiryl_c on 6/15/17.
 */
public class FileDaoCassandraImpl implements FileDao {

    public static final String
        INSERT_FILE_CQL =
        "INSERT INTO file_table (file_id,original_file_name,file_timestamp,file_body,file_user_name) VALUES (:file_id,:original_file_name,:file_timestamp,:file_body,:file_user_name);";
    public static final String
        SELECT_FILES_CQL =
        "SELECT file_id,original_file_name,file_timestamp,file_body,file_user_name FROM file_table";
    public static final String
        SELECT_FILE_BY_ID_CQL =
        "SELECT file_id,original_file_name,file_timestamp,file_body,file_user_name FROM file_table WHERE file_id=:file_id";
    private Session session;

    @Override
    public void removeFileEntityById(Long fileId) {
        session.execute(
            "DELETE FROM file_table WHERE file_id=:file_id",
            Collections.singletonMap("file_id", fileId)
        );
    }

    @Override
    public void saveFileEntity(FileEntity fileEntity) {
        fileEntity.setId(UUID.randomUUID().getLeastSignificantBits());
        fileEntity.setTimestamp(new Timestamp(System.currentTimeMillis()));
        ByteBuffer buffer = ByteBuffer.wrap(fileEntity.getBody());
        PreparedStatement ps = session.prepare(INSERT_FILE_CQL);
        BoundStatement boundStatement = new BoundStatement(ps);
        session.execute(
            boundStatement.bind(
                fileEntity.getId(),
                fileEntity.getOriginalFileName(),
                fileEntity.getTimestamp(),
                buffer,
                fileEntity.getUser().getLogin()
            )
        );
    }

    @Override
    public FileEntity getFileEntityById(Long fileId) {
        return session.execute(
            SELECT_FILE_BY_ID_CQL,
            Collections.singletonMap("file_id", fileId)
        )
            .all()
            .stream()
            .map(rowFileEntityMapper)
            .findFirst()
            .orElse(null);
    }

    @Override
    public List<FileEntity> getFileEntities() {

        return session
            .execute(SELECT_FILES_CQL)
            .all()
            .stream()
            .map(rowFileEntityMapper)
            .collect(Collectors.toList());
    }

    public void setSession(Session session) {
        this.session = session;
    }

    Function<Row, FileEntity> rowFileEntityMapper = row -> {
        User user = new User();
        user.setLogin(row.getString("file_user_name"));
        return new FileEntity(
            row.getLong("file_id"),
            row.getString("original_file_name"),
            new Timestamp(row.getTimestamp("file_timestamp").getTime()),
            row.getBytes("file_body").array(),
            user
        );
    };
}
