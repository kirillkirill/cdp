package by.br.kir.dao.hibernate;

import by.br.kir.dao.UserDao;
import by.br.kir.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by kirill-good on 9.5.17.
 */
@Repository
public class UserDaoHibernateImpl implements UserDao {

    private static final Logger LOG = LoggerFactory.getLogger(UserDaoHibernateImpl.class);

    private EntityManagerFactory emf;

    @Override
    public void createUser(User user) {
        LOG.info("createUser(login = {})", user);
        EntityManager entityManager = emf.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(user);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public User getUserByLogin(String login) {
        LOG.info("getUserByLogin(login = {})", login);
        EntityManager entityManager = emf.createEntityManager();
        try {
            return entityManager
                    .createQuery("SELECT u FROM User u WHERE u.login=:login", User.class)
                    .setParameter("login", login)
                    .getSingleResult();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
}
