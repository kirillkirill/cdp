package by.br.kir.dao;

import by.br.kir.model.User;

/**
 * Created by kirill-good on 8.5.17.
 */
public interface UserDao {

    void createUser(User user);

    User getUserByLogin(String login);
}
