package by.br.kir.dao.hibernate;

import by.br.kir.dao.FileDao;
import by.br.kir.model.FileEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by kirill-good on 9.5.17.
 */
@Repository
public class FileDaoHibernateImpl implements FileDao {

    private static final Logger LOG = LoggerFactory.getLogger(FileDaoHibernateImpl.class);

    private EntityManagerFactory emf;
    @Override
    public void removeFileEntityById(Long fileId) {
        LOG.info("removeFileEntityById(fileId = {})", fileId);
        EntityManager entityManager = emf.createEntityManager();
        try {
            FileEntity entity = new FileEntity();
            entity.setId(fileId);
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.merge(entity));
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public void saveFileEntity(FileEntity fileEntity) {
        LOG.info("saveFileEntity(fileEntity = {})", fileEntity);
        EntityManager entityManager = emf.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(fileEntity);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public FileEntity getFileEntityById(Long fileId) {
        LOG.info("getFileEntityById(fileId = {})", fileId);
        EntityManager entityManager = emf.createEntityManager();
        try {
            return entityManager
                    .createQuery("SELECT f FROM FileEntity f WHERE f.id=:fileId", FileEntity.class)
                    .setParameter("fileId", fileId)
                    .getSingleResult();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Override
    public List<FileEntity> getFileEntities() {
        LOG.info("getFileEntities()");
        EntityManager entityManager = emf.createEntityManager();
        try {
            return entityManager
                    .createQuery("SELECT f FROM FileEntity f", FileEntity.class)
                    .getResultList();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
}
