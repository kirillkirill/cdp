package by.br.kir.dao.cassandra;

import by.br.kir.dao.UserDao;
import by.br.kir.model.User;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashMap;

/**
 * Created by kiryl_c on 6/15/17.
 */
public class UserDaoCassandraImpl implements UserDao {

    public static final String
        INSERT_USER_CQL =
        "INSERT INTO user_table (user_login,user_password,user_role) VALUES (:user_login,:user_password,:user_role);";
    public static final String
        SELECT_USER_BY_LOGIN_CQL =
        "SELECT user_login,user_password,user_role FROM user_table WHERE user_login=:user_login";
    private Session session;

    @Override
    public void createUser(User user) {
        if (this.getUserByLogin(user.getLogin()) == null) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("user_login", user.getLogin());
            map.put("user_password", user.getPassword());
            map.put("user_role", user.getRole());
            session.execute(INSERT_USER_CQL, map);
        } else {
            throw new DataIntegrityViolationException("User already exists!");
        }
    }

    @Override
    public User getUserByLogin(String login) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("user_login", login);
        Row row = session.execute(SELECT_USER_BY_LOGIN_CQL, map).one();
        if (row != null) {
            User user = new User();
            user.setLogin(row.getString("user_login"));
            user.setPassword(row.getString("user_password"));
            user.setRole(row.getString("user_role"));
            return user;
        } else {
            return null;
        }
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
