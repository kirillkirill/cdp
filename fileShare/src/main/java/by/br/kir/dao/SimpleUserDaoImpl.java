package by.br.kir.dao;

import by.br.kir.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kirill-good on 9.5.17.
 */
public class SimpleUserDaoImpl implements UserDao {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleUserDaoImpl.class);

    private NamedParameterJdbcTemplate template;

    @Override
    public void createUser(User user) {
        LOG.info("createUser(user = {})", user);
        Map<String, Object> params = new HashMap<>();
        params.put("user_login", user.getLogin());
        params.put("user_password", user.getPassword());
        params.put("user_role", user.getRole());
        template.update("INSERT INTO user_table (user_login,user_password,user_role) VALUES (:user_login,:user_password,:user_role)", params);
    }

    @Override
    public User getUserByLogin(String login) {
        LOG.info("getUserByLogin(login = {})", login);
        Map<String, Object> params = new HashMap<>();
        params.put("user_login", login);
        User User = template.queryForObject("SELECT user_id,user_login,user_password,user_role FROM user_table WHERE user_login=:user_login", params, ROW_MAPPER);
        LOG.info("User = {}", User);
        return User;
    }

    private final static RowMapper<User> ROW_MAPPER = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            return new User(
                    resultSet.getLong("user_id"),
                    resultSet.getString("user_login"),
                    resultSet.getString("user_password"),
                    resultSet.getString("user_role")
            );
        }
    };

    public void setTemplate(NamedParameterJdbcTemplate template) {
        this.template = template;
    }
}
