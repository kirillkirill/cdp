package by.br.kir.dao;

import by.br.kir.model.FileEntity;
import by.br.kir.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.rowset.serial.SerialBlob;

/**
 * Created by kiryl_chepeleu on 4/12/17.
 */
public class SimpleFileDaoImpl implements FileDao {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleFileDaoImpl.class);

    public static final String SQL_DELETE_FILE = "DELETE FROM file_table WHERE file_id=:file_id";
    public static final String
        SQL_INSERT_FILE =
        "INSERT INTO file_table (original_file_name,file_timestamp,file_body,file_user_id) VALUES (:original_file_name,current_timestamp,:file_body,:file_user_id)";
    public static final String
        SQL_SELECT_FILE =
        "SELECT file_id,original_file_name,file_timestamp,file_body,file_user_id,user_login FROM file_table JOIN user_table ON file_user_id=user_id WHERE file_id=:file_id";
    public static final String
        SQL_SELECT_ALL_FILES =
        "SELECT file_id,original_file_name,file_timestamp,file_body,file_user_id,user_login FROM file_table JOIN user_table ON file_user_id=user_id";
    private NamedParameterJdbcTemplate template;

    private final static RowMapper<FileEntity> ROW_MAPPER = new RowMapper<FileEntity>() {
        @Override
        public FileEntity mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            return new FileEntity(
                resultSet.getLong("file_id"),
                resultSet.getString("original_file_name"),
                resultSet.getTimestamp("file_timestamp"),
                resultSet.getBytes("file_body"),
                new User(
                    resultSet.getLong("file_user_id"),
                    resultSet.getString("user_login"),
                    null,
                    null
                )
            );
        }
    };

    @Override
    public void removeFileEntityById(Long fileId) {
        LOG.info("removeFileEntityById(fileId = {})", fileId);
        Map<String, Object> params = new HashMap<>();
        params.put("file_id", fileId);
        template.update(SQL_DELETE_FILE, params);
    }

    @Override
    public void saveFileEntity(FileEntity fileEntity) {
        LOG.info("saveFileEntity(fileEntity = {})", fileEntity);
        Map<String, Object> params = new HashMap<>();
        params.put("original_file_name", fileEntity.getOriginalFileName());
        params.put("file_user_id", fileEntity.getUser().getId());
        try {
            params.put("file_body", new SerialBlob(fileEntity.getBody()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        template.update(SQL_INSERT_FILE, params);
    }

    @Override
    public FileEntity getFileEntityById(Long fileId) {
        LOG.info("getFileEntityById(fileId = {})", fileId);
        Map<String, Object> params = new HashMap<>();
        params.put("file_id", fileId);
        FileEntity fileEntity = template.queryForObject(SQL_SELECT_FILE, params, ROW_MAPPER);
        LOG.info("fileEntity = {}", fileEntity);
        return fileEntity;
    }

    @Override
    public List<FileEntity> getFileEntities() {
        LOG.info("getFileEntities()");
        List<FileEntity> fileEntities = template.query(SQL_SELECT_ALL_FILES, Collections.emptyMap(), ROW_MAPPER);
        LOG.info("fileEntities = {}", fileEntities);
        return fileEntities;
    }

    public void setTemplate(NamedParameterJdbcTemplate template) {
        this.template = template;
    }
}
