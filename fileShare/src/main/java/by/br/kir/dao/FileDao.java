package by.br.kir.dao;

import by.br.kir.model.FileEntity;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 4/12/17.
 */
public interface FileDao {

    void removeFileEntityById(Long fileId);

    void saveFileEntity(FileEntity fileEntity);

    FileEntity getFileEntityById(Long fileId);

    List<FileEntity> getFileEntities();
}
