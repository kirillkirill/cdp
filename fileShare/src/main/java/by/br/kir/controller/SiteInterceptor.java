package by.br.kir.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by kiryl_chepeleu on 4/29/17.
 */
@Component
public class SiteInterceptor implements HandlerInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(SiteInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o)
        throws Exception {
        LOG.info("preHandle {} {}", new Object[]{
                httpServletRequest.getMethod(),
                httpServletRequest.getContextPath()
        });
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
                           ModelAndView modelAndView) throws Exception {
        LOG.info("postHandle {} {} {}", new Object[]{
                httpServletRequest.getMethod(),
                httpServletRequest.getContextPath(),
                modelAndView
        });
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
        LOG.info("afterCompletion {} {} {}", new Object[]{
                httpServletRequest.getMethod(),
                httpServletRequest.getContextPath(),
                e
        });
    }
}
