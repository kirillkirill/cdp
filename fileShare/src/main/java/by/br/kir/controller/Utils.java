package by.br.kir.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by kirill-good on 9.5.17.
 */
public class Utils {

    public static boolean hasAdminAuthority(Authentication principal) {

        if (principal == null) {
            return false;
        } else {
            for (GrantedAuthority grantedAuthority : principal.getAuthorities()) {
                if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isValideLogin(String login) {

        return login.matches("^[a-zA-Z0-9._-]{3,}$");
    }

}
