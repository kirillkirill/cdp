package by.br.kir.controller;

import static by.br.kir.controller.Utils.hasAdminAuthority;
import static by.br.kir.controller.Utils.isValideLogin;

import by.br.kir.model.FileEntity;
import by.br.kir.model.User;
import by.br.kir.service.FileService;
import by.br.kir.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController {

    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private FileService fileService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(HttpServletResponse response, Authentication principal) throws IOException {

        LOG.info("GET /");
        LOG.info("principal = {}", principal);
        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.getModel().put("files", fileService.getFileEntities());
        modelAndView.getModel().put("isAuthenticated", principal == null ? false : principal.isAuthenticated());
        modelAndView.getModel().put("isAdmin", hasAdminAuthority(principal));
        modelAndView.getModel().put("userName", principal.getName());
        return modelAndView;
    }

    @RequestMapping(value = "/download/{fileId}", method = RequestMethod.GET)
    public ResponseEntity download(HttpServletResponse response, @PathVariable("fileId") Long fileId)
            throws IOException {

        LOG.info("GET /download/{}", fileId);
        FileEntity fileEntity = fileService.getFileEntityById(fileId);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileEntity.getOriginalFileName());
        return new ResponseEntity(
                fileEntity.getBody(),
                headers,
                HttpStatus.OK
        );
    }

    @RequestMapping(value = "/upload",
            method = RequestMethod.POST)
    public String upload(@RequestParam("file") MultipartFile file, Authentication principal) throws IOException {

        LOG.info("POST /upload");
        LOG.info("principal.getName() = {}", principal.getName());
        if (file.getSize() != 0) {
            fileService.saveFile(
                    new FileEntity(
                        null,
                        file.getOriginalFilename(),
                        null,
                        file.getBytes(),
                        userService.getUserByLogin(principal.getName())
                    )
            );
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/admin/remove/{fileId}", method = RequestMethod.GET)
    public String remove(@PathVariable Long fileId) {

        LOG.info("remove(fileId = {})", fileId);
        fileService.removeFileEntityById(fileId);
        return "redirect:/";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {

        LOG.info("logout");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public ModelAndView reg(@RequestParam("username") String login,
                            @RequestParam("password") String password) {
        LOG.info("reg");
        ModelAndView modelAndView = new ModelAndView("login");
        if (isValideLogin(login)) {
            userService.createUser(new User(null, login, password, "ROLE_USER"));
            modelAndView.getModel().put("message", "User was created successfully!");
        } else {
            modelAndView.getModel().put("message", "Invalid user login, try only letters, digits and _-., at least 3 chars");
        }
        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        LOG.error("Request: " + req.getRequestURL() + " raised " + ex);
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex.toString());
        mav.addObject("url", req.getRequestURL().toString());
        mav.setViewName("error");
        return mav;
    }
}
