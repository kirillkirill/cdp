package by.br.kir.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by kiryl_chepeleu on 4/11/17.
 */
@Getter
@Setter
@ToString(exclude = "body")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode

@Entity
@Table(name = "file_table")
@DynamicInsert
public class FileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private Long id;
    @Column(name = "original_file_name")
    private String originalFileName;
    @Column(name = "file_timestamp")
    private Timestamp timestamp;
    @Column(name = "file_body")
    private byte[] body;
    @JoinColumn(name = "file_user_id",
        nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
}
