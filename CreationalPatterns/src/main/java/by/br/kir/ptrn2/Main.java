package by.br.kir.ptrn2;

import by.br.kir.ptrn2.wrapper.Stack;
import by.br.kir.ptrn2.wrapper.StackList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by kiryl_chepeleu on 3/9/17.
 */
public class Main {

    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Stack<String> stringStack = new StackList<>(new LinkedList<>());
        Scanner scanner = new Scanner(System.in);
        String line;
        LOG.info("Press enter");
        while ((line = scanner.nextLine()) != null) {
            switch (line) {
                case "push":
                    LOG.info("Type line to push");
                    stringStack.push(scanner.nextLine());
                    break;
                case "pop":
                    LOG.info("pop = {}", stringStack.pop());
                    break;
            }
            LOG.info("Type:\npush\npop\n");
        }
    }
}
