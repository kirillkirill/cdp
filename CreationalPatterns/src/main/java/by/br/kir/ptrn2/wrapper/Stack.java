package by.br.kir.ptrn2.wrapper;

/**
 * Created by kiryl_chepeleu on 3/9/17.
 */
public interface Stack<T> {
    void push(T t);
    T pop();
}
