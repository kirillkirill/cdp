package by.br.kir.ptrn2.wrapper;

import by.br.kir.ptrn2.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/9/17.
 */
public class StackList<T> implements Stack<T> {

    private static final Logger LOG = LogManager.getLogger(StackList.class);

    private List<T> list;

    public StackList(List<T> list) {
        this.list = list;
    }

    @Override
    public void push(T t) {
        LOG.debug("pushed value = {}", t);
        list.add(t);
    }

    @Override
    public T pop() {
        if (list.size() != 0) {
            T t = list.get(0);
            list.remove(0);
            LOG.debug("pop value = {}", t);
            return t;
        } else {
            LOG.debug("stack empty, pop value = null");
            return null;
        }
    }
}
