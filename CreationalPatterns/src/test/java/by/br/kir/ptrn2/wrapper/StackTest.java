package by.br.kir.ptrn2.wrapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 3/9/17.
 */
@RunWith(Parameterized.class)
public class StackTest {

    @Parameterized.Parameters
    public static Collection<List> data() {
        return Arrays.asList(new ArrayList(), new LinkedList());
    }

    @Parameterized.Parameter
    public List list;

    private Stack<Integer> stack;

    @Before
    public void setUp() {
        stack = new StackList<>(list);
    }

    @Test
    public void pushTest() {
        final Integer zero = 0;
        stack.push(zero);
        Assert.assertEquals(zero, list.get(0));
    }

    @Test
    public void popTest() {
        final Integer zero = 0;
        stack.push(zero);
        Assert.assertEquals(zero, stack.pop());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void bigTest() {
        final Integer[] integers = {1, 2, 3, 4, 5};
        for (int i = 0; i < integers.length; i++) {
            stack.push(integers[i]);
            Assert.assertEquals(integers[i], list.get(i));
            Assert.assertEquals(i + 1, list.size());
        }
        for (int i = 0; i < integers.length; i++) {
            Assert.assertEquals(integers[i], stack.pop());
        }
    }
}
