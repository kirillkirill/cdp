Create a console program for managing "TODO" list.

Requirements:
- the following commads should be supported: add new task, show all taks, remove specific task, remove all tasks
- storage for tasks should be text file
- exceptions and incorrect input should be hadled properly


Implementation notes:
- upon designing keep in mind that requirements may change later e.g. database as storage for tasks, logging into files, new functionality can be added, etc.
- follow software development principles such as DRY, SOLID, KISS, etc.