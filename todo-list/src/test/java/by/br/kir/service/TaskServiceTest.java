package by.br.kir.service;

import by.br.kir.dao.FastTaskDaoCsvFile;
import by.br.kir.dao.TaskDao;
import by.br.kir.dao.TaskDaoCsvFile;
import by.br.kir.model.Task;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class TaskServiceTest {

    @Test
    public void clearGetAllTest() throws IOException {

        File dataFile = getTmpFile();
        testServiceGetAllTest(new TaskDaoCsvFile(dataFile));
        testServiceGetAllTest(new FastTaskDaoCsvFile(dataFile));
    }

    private File getTmpFile() throws IOException {
        File tempFile = File.createTempFile("todo", "list");
        tempFile.deleteOnExit();
        return tempFile;
    }

    private void testServiceGetAllTest(TaskDao taskDao) {
        TaskService taskService = new TaskServiceImpl(taskDao);
        taskService.removeAllTasks();
        Assert.assertEquals(taskService.getAllTasks().size(), 0);
    }

    @Test
    public void addGetAllDeleteTest() throws IOException {

        testServiceAddGetAllDeleteTest(new TaskServiceImpl(new TaskDaoCsvFile(getTmpFile())));
        testServiceAddGetAllDeleteTest(new TaskServiceImpl(new FastTaskDaoCsvFile(getTmpFile())));
    }

    private void testServiceAddGetAllDeleteTest(TaskService taskService) {
        taskService.addTask(new Task("123", "456"));
        taskService.addTask(new Task("abc", "def"));
        Assert.assertEquals(2, taskService.getAllTasks().size());
        Assert.assertEquals(taskService.getAllTasks().get(0).getDescription(), "456");
        Assert.assertEquals(taskService.getAllTasks().get(1).getDescription(), "def");
        taskService.removeTask(new Task(taskService.getAllTasks().get(0).getId(), null));
        Assert.assertEquals(taskService.getAllTasks().get(0).getDescription(), "def");
    }
}
