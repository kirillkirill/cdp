package by.br.kir.dao;

import by.br.kir.model.Task;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kiryl_chepeleu on 2/10/17.
 */
public class DaoPerformanceTest {

    @Test
    public void taskDaoCsvFileAddTaskTest() throws IOException {
        System.out.println("*************************");
        System.out.println("taskDaoCsvFileAddTaskTest");
        List<TaskDao> list = Arrays.asList(new TaskDaoCsvFile(getTempFile()), new FastTaskDaoCsvFile(getTempFile()));
        final int n = 1000;
        for (TaskDao taskDao : list) {
            long time = testDao(o -> {
                taskDao.addTask(new Task("id", "desc"));
                return null;
            }, n);
            System.out.println("taskDao.getClass().getName() = " + taskDao.getClass().getName());
            System.out.println("time = " + time);
            System.out.println();
        }
        System.out.println("*************************");
    }

    @Test
    public void taskDaoCsvFileRemoveTaskTest() throws IOException {
        System.out.println("****************************");
        System.out.println("taskDaoCsvFileRemoveTaskTest");
        TaskDaoCsvFile taskDaoCsvFile = new TaskDaoCsvFile(getTempFile());
        FastTaskDaoCsvFile fastTaskDaoCsvFile = new FastTaskDaoCsvFile(getTempFile());

        final int n = 100;
        List<Task> data = IntStream
            .range(0, n)
            .mapToObj(value -> new Task(Integer.toString(value), UUID.randomUUID().toString()))
            .collect(Collectors.toList());
        taskDaoCsvFile.write(data);
        fastTaskDaoCsvFile.write(data);
        List<TaskDao> list = Arrays.asList(fastTaskDaoCsvFile, taskDaoCsvFile);
        for (TaskDao taskDao : list) {
            AtomicInteger c = new AtomicInteger(0);
            long time = testDao(o -> {
                taskDao.removeTask(data.get(c.getAndIncrement()));
                return null;
            }, n);
            System.out.println("taskDao.getClass().getName() = " + taskDao.getClass().getName());
            System.out.println("time = " + time);
            System.out.println();
        }
        System.out.println("****************************");
    }

    private File getTempFile() throws IOException {
        File tempFile = File.createTempFile("todo", "list");
        tempFile.deleteOnExit();
        return tempFile;
    }

    private long testDao(final Function<TaskDao, Object> function, final int n) {
        final long t0 = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            function.apply(null);
        }
        final long t1 = System.currentTimeMillis();
        return t1 - t0;
    }
}
