package by.br.kir.dao;

import by.br.kir.model.Task;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class TaskDaoCsvImplTest {

    @Test
    public void csvTaskFileTest() throws IOException {
        TaskDaoCsvFile taskDaoCsvFile = new TaskDaoCsvFile(File.createTempFile("todo", "list"));
        List<Task> tasks = Arrays.asList(new Task("1", "11"), new Task("2", "22"));
        taskDaoCsvFile.write(tasks);
        List<Task> readTasks = taskDaoCsvFile.read();
        Assert.assertEquals(2, readTasks.size());
        Assert.assertEquals("1", readTasks.get(0).getId());
        Assert.assertEquals("11", readTasks.get(0).getDescription());
        Assert.assertEquals("2", readTasks.get(1).getId());
        Assert.assertEquals("22", readTasks.get(1).getDescription());
        Assert.assertEquals(tasks, readTasks);
    }
}
