package by.br.kir.controller;

import by.br.kir.service.TaskService;
import org.apache.commons.cli.Option;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class CommandLineActionResolverTest {

    @Test
    public void test() {
        CommandLineActionResolver resolver = new CommandLineActionResolver();
        Action action1 = new Action() {
            @Override
            public void perform(TaskService taskService) {
            }
        };
        Action action2 = new Action() {
            @Override
            public void perform(TaskService taskService) {
            }
        };
        Option option1 = new Option("op1", "des1");
        Option option2 = new Option("op2", "des2");
        resolver.add(option1, action1);
        resolver.add(option2, action2);
        Assert.assertTrue(resolver.getOptions().getOptions().contains(option1));
        Assert.assertTrue(resolver.getOptions().getOptions().contains(option2));
        Assert.assertEquals(action1, resolver.resolve(option1));
        Assert.assertEquals(action2, resolver.resolve(option2));
    }
}
