package by.br.kir;

import by.br.kir.controller.CommandLineActionResolver;
import by.br.kir.controller.CommandLineController;
import by.br.kir.dao.TaskDaoCsvFile;
import by.br.kir.model.Task;
import by.br.kir.service.TaskService;
import by.br.kir.service.TaskServiceImpl;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class Main {

    public static void main(String[] args) throws ParseException {

        CommandLineActionResolver actionResolver = initActionResolver();
        File file = getDataFile();
        TaskService taskService = new TaskServiceImpl(new TaskDaoCsvFile(file));
        CommandLineController controller = new CommandLineController(actionResolver, taskService);
        controller.perform(args);
    }

    private static File getDataFile() {
        String homeDir = System.getProperty("user.home");
        File file = new File(homeDir, ".todo-list.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return file;
    }

    private static CommandLineActionResolver initActionResolver() {
        CommandLineActionResolver actionResolver = new CommandLineActionResolver();
        Option list = new Option("l", "list", false, "show list of all tasks");
        Option add = new Option("a", "add", false, "add task");
        Option delete = new Option("d", "delete", false, "delete task");
        Option clear = new Option("c", "clear", false, "delete all tasks");
        Option help = new Option("h", "help", false, "usage");

        Scanner scanner = new Scanner(System.in);

        actionResolver.add(
            list,
            taskService ->
                taskService
                    .getAllTasks()
                    .forEach(task -> System.out.println(task.getId() + " " + task.getDescription()))
        );
        actionResolver.add(add, taskService -> {
            System.out.println("Add");
            System.out.println("Type description:");
            taskService.addTask(new Task(null, scanner.nextLine()));
        });
        actionResolver.add(delete, taskService -> {
            System.out.println("Delete");
            System.out.println("Type id:");
            taskService.removeTask(new Task(scanner.nextLine(), null));
        });
        actionResolver.add(clear, taskService -> {
            System.out.println("Delete all tasks");
            taskService.removeAllTasks();
        });
        actionResolver.add(help, taskService -> {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("todo-list", actionResolver.getOptions());
        });
        return actionResolver;
    }

}
