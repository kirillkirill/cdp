package by.br.kir.dao;

import by.br.kir.model.Task;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class TaskDaoCsvFile implements TaskDao {

    public static final int ID_COLUMN = 0;
    public static final int DESC_COLUMN = 1;
    protected File file;

    public TaskDaoCsvFile(File file) {
        this.file = file;
    }

    protected List<Task> read() {
        try (Reader in = new FileReader(file)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
            List<Task> result = new LinkedList<>();
            for (CSVRecord record : records) {
                String columnId = record.get(ID_COLUMN);
                String columnDescription = record.get(DESC_COLUMN);
                result.add(new Task(columnId, columnDescription));
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void write(List<Task> tasks) {
        try (FileWriter out = new FileWriter(file)) {

            CSVPrinter printer = CSVFormat.DEFAULT.print(out);
            for (Task task : tasks) {
                printer.printRecord(task.getId(), task.getDescription());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addTask(Task task) {
        generateId(task);
        List<Task> tasks = this.read();
        tasks.add(task);
        this.write(tasks);
    }

    protected void generateId(Task task) {
        task.setId(UUID.randomUUID().toString().replaceAll("-", ""));
    }

    @Override
    public List<Task> getAllTasks() {
        return this.read();
    }

    @Override
    public void removeTask(Task task) {
        List<Task> tasks = this.read();
        tasks.removeIf(i -> i.getId().equals(task.getId()));
        this.write(tasks);
    }

    @Override
    public void removeAllTasks() {
        this.write(Collections.EMPTY_LIST);
    }
}
