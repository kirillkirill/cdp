package by.br.kir.dao;

import by.br.kir.model.Task;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public interface TaskDao {

    void addTask(Task task);

    List<Task> getAllTasks();

    void removeTask(Task task);

    void removeAllTasks();
}
