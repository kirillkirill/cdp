package by.br.kir.dao;

import by.br.kir.model.Task;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiryl_chepeleu on 2/10/17.
 */
public class FastTaskDaoCsvFile extends TaskDaoCsvFile {

    public static final int SPACE_CHAR = 32;
    public static final int UUID_LEN = 32;

    public FastTaskDaoCsvFile(File file) {
        super(file);
    }

    @Override
    public void addTask(Task task) {
        generateId(task);
        append(task);
    }

    private void append(Task task) {
        try (FileWriter out = new FileWriter(file, true)) {
            CSVPrinter printer = CSVFormat.DEFAULT.print(out);
            printer.printRecord(task.getId(), task.getDescription());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void removeTask(Task task) {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
            for (; ; ) {
                long filePointer = randomAccessFile.getFilePointer();
                String line = randomAccessFile.readLine();
                if (line == null) {
                    break;
                }
                Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new StringReader(line));
                Iterator<CSVRecord> iterator = records.iterator();
                CSVRecord next = iterator.next();
                Task taskFromLine = new Task(next.get(ID_COLUMN), next.get(DESC_COLUMN));
                if (taskFromLine.getId().equals(task.getId())) {
                    randomAccessFile.seek(filePointer);
                    randomAccessFile.write(SPACE_CHAR);
                    randomAccessFile.close();
                    return;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Task> getAllTasks() {
        List<Task> allTasks = super.getAllTasks();
        allTasks.removeIf(task -> task.getId().trim().length() != UUID_LEN);
        return allTasks;
    }
}
