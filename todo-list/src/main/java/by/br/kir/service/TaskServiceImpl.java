package by.br.kir.service;

import by.br.kir.dao.TaskDao;
import by.br.kir.model.Task;

import java.util.List;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class TaskServiceImpl implements TaskService {

    private TaskDao taskDao;

    public TaskServiceImpl(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    public void addTask(Task task) {
        taskDao.addTask(task);
    }

    public List<Task> getAllTasks() {
        return taskDao.getAllTasks();
    }

    public void removeTask(Task task) {
        taskDao.removeTask(task);
    }

    public void removeAllTasks() {
        taskDao.removeAllTasks();
    }
}
