package by.br.kir.controller;

import by.br.kir.service.TaskService;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public interface Action {

    void perform(TaskService taskService);
}
