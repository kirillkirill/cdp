package by.br.kir.controller;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class CommandLineActionResolver {

    private Options options = new Options();
    private Map<Option, Action> optionActionMap = new HashMap<>();

    public void add(Option option, Action action) {
        options.addOption(option);
        optionActionMap.put(option, action);
    }

    public Action resolve(Option option) {
        return optionActionMap.get(option);
    }

    public Options getOptions() {
        return options;
    }
}
