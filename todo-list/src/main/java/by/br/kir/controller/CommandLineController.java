package by.br.kir.controller;

import by.br.kir.service.TaskService;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

/**
 * Created by kiryl_chepeleu on 2/9/17.
 */
public class CommandLineController {

    private CommandLineActionResolver actionResolver;
    private TaskService taskService;

    public CommandLineController(CommandLineActionResolver actionResolver, TaskService taskService) {
        this.actionResolver = actionResolver;
        this.taskService = taskService;
    }

    public void perform(String[] args) {
        CommandLineParser parser = new BasicParser();
        try {
            CommandLine cmd = parser.parse(actionResolver.getOptions(), args);
            for (Option option : cmd.getOptions()) {
                actionResolver.resolve(option).perform(taskService);
            }
        } catch (UnrecognizedOptionException e) {
            System.out.println(e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
